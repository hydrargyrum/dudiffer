#!/usr/bin/env python3
# SPDX-License-Identifier: WTFPL

import argparse
import signal
import threading
from pathlib import Path


Entry = tuple[int, "Tree | None"]
Tree = dict[str, Entry]


def parse_int_suffix(s: str) -> int:
    try:
        return int(s)
    except ValueError:
        pass

    s = s.lower()
    for suffix, factor in SUFFIX_PARSE.items():
        if s.endswith(suffix):
            return int(s[:-1]) * factor


def build_structure(root: Path) -> Tree:
    ret: Tree = {}
    subs = []
    try:
        subs = [
            sub for sub in root.iterdir()
            if not sub.is_symlink()
            and (sub.is_dir() or sub.is_file())
        ]
    except PermissionError:
        return ret

    for sub in subs:
        if sub.is_file():
            ret[sub.name] = (sub.stat().st_size, None)
        elif sub.is_dir():
            substructure = build_structure(sub)
            ret[sub.name] = (
                sum(sz for sz, _ in substructure.values()),
                substructure
            )

    return ret


def background_build(path: Path, output: list[Tree]) -> None:
    output.append(build_structure(path))


SUFFIXES = ["B", "KiB", "MiB", "GiB", "TiB"]
SUFFIX_PARSE = {
    "k": 10 ** 3,
    "m": 10 ** 6,
    "g": 10 ** 9,
    "t": 10 ** 12,
}


def humansize(sz: int | float) -> str:
    for suffix in SUFFIXES:
        if abs(sz) < 1024:
            break
        sz /= 1024
    return f"{sz:+.2f} {suffix}"


def print_tree(astruct, bstruct, level: int = 0) -> None:
    akeys = set(astruct)
    bkeys = set(bstruct)

    def sortfunc(d, k: str) -> int:
        try:
            return d[k][0]
        except KeyError:
            return 0

    def keyfunc(k: str) -> int:
        return abs(sortfunc(astruct, k) - sortfunc(bstruct, k))

    for k in sorted(akeys | bkeys, key=keyfunc, reverse=True):
        if k not in astruct:
            diff = bstruct[k][0]
            is_dir = False
        elif k not in bstruct:
            diff = -astruct[k][0]
            is_dir = False
        else:
            diff = bstruct[k][0] - astruct[k][0]
            is_dir = bool(astruct[k][1])

        if abs(diff) < args.ignore_diff_smaller_than:
            continue

        sep = ""
        if is_dir:
            sep = "/"
        print("  " * level, f"{k}{sep}".encode(errors="replace").decode(), humansize(diff))

        if is_dir:
            print_tree(astruct[k][1], bstruct[k][1], level + 1)


if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    signal.signal(signal.SIGPIPE, signal.SIG_DFL)

    aparser = argparse.ArgumentParser()
    # 0 means: ignore nothing
    # 1 means: ignore when same size
    # 100_000: ignore when diff is smaller than 100kB
    aparser.add_argument("--ignore-diff-smaller-than", type=parse_int_suffix, default=0)
    aparser.add_argument("dir_a", type=Path)
    aparser.add_argument("dir_b", type=Path)
    args = aparser.parse_args()

    # start A
    thread_out: list[Tree] = []
    thread = threading.Thread(target=background_build, args=(args.dir_a, thread_out))
    thread.start()

    b = build_structure(args.dir_b)

    # finish A
    thread.join()
    a = thread_out[0]

    print_tree(a, b)
